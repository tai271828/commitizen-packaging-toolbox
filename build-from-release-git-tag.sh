#!/usr/bin/env bash
PKG_NAME="commitizen"
WORKING_DIR=${HOME}/packaging-deb-${PKG_NAME}
UPSTREAM_VERSION="2.27.1"
DEBIAN_PKG_VERSION_SUFFIX="1"
PKG_DEBMAKE_WORKING_DIR_NAME=${PKG_NAME}-${UPSTREAM_VERSION}
PKG_DEBMAKE_WORKING_DIR=${WORKING_DIR}/${PKG_DEBMAKE_WORKING_DIR_NAME}
#UPSTREAM_GIT_REPO_URL=https://github.com/commitizen-tools/${PKG_NAME}.git
UPSTREAM_GIT_REPO_URL=https://github.com/tai271828/${PKG_NAME}.git

set -x

mkdir ${WORKING_DIR}

# directory format {PKG_NAME}-${UPSTREAM_VERSION} is a must for debmake
git clone --no-checkout -o upstream ${UPSTREAM_GIT_REPO_URL} ${PKG_DEBMAKE_WORKING_DIR}

pushd ${PKG_DEBMAKE_WORKING_DIR}

git checkout -b debian/sid v${UPSTREAM_VERSION}

# (NOT NEEDED because we use dh_make --createorig)
# prepare release tarball for a python package
#
# by using poetry the "upstream file may change" issue will happen since it differs from upstream source of git branch
#poetry build
#mv dist/${PKG_DEBMAKE_WORKING_DIR_NAME}.tar.gz ../
# use git archive directly
#git archive --format=tar.gz -o ../${PKG_DEBMAKE_WORKING_DIR_NAME}.tar.gz v${UPSTREAM_VERSION}
#dh_make --createorig --python --yes
# you may use template to handle the customization as well:
dh_make --createorig --python --yes -t ${HOME}/toolbox/scripts/packaging-deb-${PKG_NAME}/debian
# TODO not sure why not executable
chmod +x debian/rules

# do not want to use pybuild because it seems pybuild would try to find and use setup.py to build but commitizen uses
# poetry
# pybuild is necessary to build python package
# and debian python package / pybuild assumes setup.py exists https://wiki.debian.org/Python/LibraryStyleGuide
#sed -i "s@ --buildsystem=pybuild@@" debian/rules
# have no doc prepared yet
# and it has README.Debian not found. weird. relative path incorrect?
#rm -f debian/python-commitizen-doc.docs debian/README.Debian debian/README.source

# conventional manipulation to debianize python source
#
# dh_make generates more complicated or "raw" debian/* files to edit... headache, use debmake instead
# oh no, the template content of "dh_make --createorig" seems more accurate. let's use dh_make

## explicitly tell debmake that we want to generate python3 binary
#debmake -b:"python3"

git add ./debian
git commit -m "debianize the upstream source"

# s of 'v%(version)s' stands for string?
gbp buildpackage --git-ignore-new --git-builder=sbuild \
    --extra-package=/home/ubuntu/packaging-deb-questionary/python3-questionary_1.9.0+dfsg-1_all.deb \
    --extra-package=/home/ubuntu/packaging-deb-decli/python3-decli_0.5.2+dfsg-1_all.deb \
    --extra-package=/home/ubuntu/packaging-deb-pytest-freezegun/python3-pytest-freezegun_0.4.2-1_all.deb \
    --git-pristine-tar --git-pristine-tar-commit \
    --git-upstream-tag='v%(version)s' --git-debian-branch=debian/sid

popd
