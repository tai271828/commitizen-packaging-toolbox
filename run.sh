#!/usr/bin/env bash

set -e
set -x

# first thing first, build all other dependencies
ROOT_DIR_HELPER_SCRIPTS=$( dirname "${BASH_SOURCE[0]}" )/..
DEP_COMMITIZEN_DIR=${ROOT_DIR_HELPER_SCRIPTS}/commitizen-packaging-toolbox
DEP_DECLI_DIR=${ROOT_DIR_HELPER_SCRIPTS}/decli-packaging-toolbox
DEP_QUESTIONARY_DIR=${ROOT_DIR_HELPER_SCRIPTS}/questionary-packaging-toolbox
DEP_PYTEST_FREEZEGUN_DIR=${ROOT_DIR_HELPER_SCRIPTS}/pytest-freezegun-packaging-toolbox

# build decli
"${DEP_DECLI_DIR}"/build-from-release-git-tag.sh

# build questionary
"${DEP_QUESTIONARY_DIR}"/build-from-release-git-tag.sh

# build pytest-freezegun
"${DEP_PYTEST_FREEZEGUN_DIR}"/build-from-release-git-tag.sh

# finally, build our target
"${DEP_COMMITIZEN_DIR}"/build-from-release-git-tag.sh
