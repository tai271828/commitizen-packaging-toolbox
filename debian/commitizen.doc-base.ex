Document: commitizen
Title: Debian commitizen Manual
Author: <insert document author here>
Abstract: This manual describes what commitizen is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/commitizen/commitizen.sgml.gz

Format: postscript
Files: /usr/share/doc/commitizen/commitizen.ps.gz

Format: text
Files: /usr/share/doc/commitizen/commitizen.text.gz

Format: HTML
Index: /usr/share/doc/commitizen/html/index.html
Files: /usr/share/doc/commitizen/html/*.html
