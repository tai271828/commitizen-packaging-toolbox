#!/usr/bin/env bash
# prepare development environment of packaging with latest gbp for less bugs
#
# we may not need the gbp in poetry venv in this script since gbp in debian sid and ubuntu jammy is new enough
#
SCRIPT_ENTRY_PATH="$( realpath "${BASH_SOURCE[0]}" )"
SCRIPT_DIR="$( dirname "${SCRIPT_ENTRY_PATH}" )"
# install and setup build env
#../packaging-deb-prepare/01-schroot.sh debian arm64 bullseye

sudo apt install -y build-essential debhelper devscripts equivs
# poetry is not ready around ubuntu jammy. you may prepare your own poetry instead
sudo apt install -y python3-poetry
sudo apt install -y pristine-tar
# for python deb packaging
sudo apt install -y dh-python python3-all



# will be used by poetry. seems a packaging bug
pip install CacheControl
export PATH=${HOME}/.local/bin:$PATH

mkdir -p ${WORKING_DIR}

pushd ${WORKING_DIR}
# prepare python venv for the installation of the latest gbp (0.9.25)
# so we can avoid issue like https://www.mail-archive.com/debian-bugs-dist@lists.debian.org/msg1821761.html
# when importing dsc with an empty directory
#
# tested distribution: ubuntu jammy
poetry init --no-interaction
poetry add gbp
poetry run ${SCRIPT_DIR}/build-from-release-git-tag_run-in-venv.sh

popd
