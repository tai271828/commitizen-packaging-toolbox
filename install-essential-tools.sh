#!/usr/bin/env bash
#
# one-off task at the moment of the first build to prepare essential tools
#

set -e
set -x

# dh-python for
# you may need to install the Debian::Debhelper::Sequence::python3 module
sudo apt install dh-make dh-python git-buildpackage

#../../packaging-deb-00-prepare/01-schroot.sh debian arm64 sid
